#include "MainWindow.h"

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent)
{
    setupUi(this);

/*

  the structure of the main window's central widget

 *---------------------------------------------------*
 |                 central widget                    |
 |                                                   |
 |  *---------------------------*-----------------*  |
 |  |     rendering context     |   scroll area   |  |
 |  |       OpenGL widget       | *-------------* |  |
 |  |                           | | side widget | |  |
 |  |                           | |             | |  |
 |  |                           | |             | |  |
 |  |                           | *-------------* |  |
 |  *---------------------------*-----------------*  |
 |                                                   |
 *---------------------------------------------------*

*/
    _side_widget = new SideWidget(this);

    _scroll_area = new QScrollArea(this);
    _scroll_area->setWidget(_side_widget);
    _scroll_area->setSizePolicy(_side_widget->sizePolicy());
    _scroll_area->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

    _gl_widget = new GLWidget(this);

    centralWidget()->setLayout(new QHBoxLayout());
    centralWidget()->layout()->addWidget(_gl_widget);
    centralWidget()->layout()->addWidget(_scroll_area);

    // creating a signal slot mechanism between the rendering context and the side widget
    connect(_side_widget->rotate_x_slider, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_x(int)));
    connect(_side_widget->rotate_y_slider, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_y(int)));
    connect(_side_widget->rotate_z_slider, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_z(int)));

    connect(_side_widget->zoom_factor_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_zoom_factor(double)));

    connect(_side_widget->trans_x_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_x(double)));
    connect(_side_widget->trans_y_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_y(double)));
    connect(_side_widget->trans_z_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_z(double)));

    connect(_side_widget->curve_combo, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_curve_index(int)));
    connect(_side_widget->surface_combo, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_surface_index(int)));

    connect(_side_widget->checkBox_curves,SIGNAL(toggled(bool)), _gl_widget,SLOT(toggle_curves(bool)));
    connect(_side_widget->checkBox_surfaces,SIGNAL(toggled(bool)), _gl_widget,SLOT(toggle_surfaces(bool)));
    connect(_side_widget->checkBox_mouse,SIGNAL(stateChanged(int)), _gl_widget,SLOT(toggle_mouse(int)));
    connect(_side_widget->checkBox_project,SIGNAL(toggled(bool)), _gl_widget,SLOT(toggle_project(bool)));

    //connect(_side_widget->insert_patch,SIGNAL(released()),_gl_widget,SLOT(insert_new_patch()));
    //connect(_side_widget->move_patch, SIGNAL(released()),_gl_widget, SLOT(move_new_patch()));

    connect(_side_widget->patch_bottom, SIGNAL(released()),_gl_widget, SLOT(insert_patch_bottom()));
    connect(_side_widget->patch_bottom_right, SIGNAL(released()),_gl_widget, SLOT(insert_patch_bottom_right()));
    connect(_side_widget->patch_right, SIGNAL(released()), _gl_widget, SLOT(insert_patch_right()));
    connect(_side_widget->patch_top_right, SIGNAL(released()),_gl_widget, SLOT(insert_patch_top_right()));
    connect(_side_widget->patch_top, SIGNAL(released()),_gl_widget, SLOT(insert_patch_top()));
    connect(_side_widget->patch_top_left, SIGNAL(released()),_gl_widget, SLOT(insert_patch_top_left()));
    connect(_side_widget->patch_left, SIGNAL(released()),_gl_widget, SLOT(insert_patch_left()));
    connect(_side_widget->patch_bottom_left, SIGNAL(released()),_gl_widget, SLOT(insert_patch_bottom_left()));

    connect(_side_widget->cp_x, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_control_point_x(double)));
    connect(_side_widget->cp_y, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_control_point_y(double)));
    connect(_side_widget->cp_z, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_control_point_z(double)));
    connect(_side_widget->control_point_id, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_id_control_point(int)));

    connect(_gl_widget, SIGNAL(control_point_changed_x(double)), _side_widget->cp_x, SLOT(setValue(double)));
    connect(_gl_widget, SIGNAL(control_point_changed_y(double)), _side_widget->cp_y, SLOT(setValue(double)));
    connect(_gl_widget, SIGNAL(control_point_changed_z(double)), _side_widget->cp_z, SLOT(setValue(double)));

}

//--------------------------------
// implementation of private slots
//--------------------------------
void MainWindow::on_action_Quit_triggered()
{
    qApp->exit(0);
}
