#pragma once
#include "../Core/TensorProductSurfaces3.h"

namespace cagd
{
    class BicubicBezierPatch: public TensorProductSurface3
    {
    protected:
        GLdouble u_knot, v_knot;
    public:

        BicubicBezierPatch();
        GLboolean UBlendingFunctionValues(GLdouble u_knot, RowMatrix<GLdouble>& blending_values) const;
        GLboolean VBlendingFunctionValues(GLdouble v_knot, RowMatrix<GLdouble>& blending_values) const;
        GLboolean CalculatePartialDerivatives(GLdouble u, GLdouble v, PartialDerivatives& pd) const;
        GLdouble getU_knot();
    };
}
