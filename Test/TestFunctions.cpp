#include <cmath>
#include "TestFunctions.h"
#include "Core/Constants.h"

using namespace cagd;
using namespace std;

GLdouble spiral_on_cone::u_min = -6.2831;
GLdouble spiral_on_cone::u_max = +6.2831;

GLdouble trefoil_knot::u_min = 0;
GLdouble trefoil_knot::u_max = 2*PI;

GLdouble epicycloid::u_min = 0;
GLdouble epicycloid::u_max = 2*PI;
GLdouble epicycloid::r = 3;
GLdouble epicycloid::k = 1;

GLdouble hypocycloid::u_min = 0;
GLdouble hypocycloid::u_max = 2*PI;
GLdouble hypocycloid::r = 3;
GLdouble hypocycloid::k = 4;

GLdouble viviani::u_min = -2*PI;
GLdouble viviani::u_max = 2*PI;
GLdouble viviani::a = 1;

GLdouble torus::u_min = 0;
GLdouble torus::u_max = 2*PI;
GLdouble torus::v_min = 0;
GLdouble torus::v_max = 2*PI;
GLdouble torus::R = 10;
GLdouble torus::r = 5;

GLdouble torus_circle::u_min = 0;
GLdouble torus_circle::u_max = PI;
GLdouble torus_circle::v_min = 0;
GLdouble torus_circle::v_max = TWO_PI - PI / 4;
GLdouble torus_circle::R = 0;
GLdouble torus_circle::r = 1;

GLdouble alfred::u_min = 0;
GLdouble alfred::u_max = 2*PI;
GLdouble alfred::v_min = 0;
GLdouble alfred::v_max = 2*PI;

GLdouble helicoid::u_min = 0;
GLdouble helicoid::u_max = 2*PI;
GLdouble helicoid::v_min = 0;
GLdouble helicoid::v_max = 2*PI;

GLdouble hybrid::u_min = 0;
GLdouble hybrid::u_max = 3;
GLdouble hybrid::v_min = 0;
GLdouble hybrid::v_max = TWO_PI;

//spiral on cone
DCoordinate3 spiral_on_cone::d0(GLdouble u)
{
    return DCoordinate3(u * cos(u), u * sin(u), u);
}

DCoordinate3 spiral_on_cone::d1(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(c - u * s, s + u * c, 1.0);
}

DCoordinate3 spiral_on_cone::d2(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(-2.0 * s - u * c, 2.0 * c - u * s, 0);
}

//trefoil_knot
DCoordinate3 trefoil_knot::d0(GLdouble u)
{
    return DCoordinate3(sin(u)+2*sin(2*u),cos(u)-2*cos(2*u),-sin(3*u));
}

DCoordinate3 trefoil_knot::d1(GLdouble u)
{
    //GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(cos(u)+4*cos(2*u),-sin(u)+4*sin(2*u),-3*cos(3*u));
}

DCoordinate3 trefoil_knot::d2(GLdouble u)
{
    //GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(-sin(u)-8*sin(2*u),-cos(u)+8*cos(2*u),9*sin(3*u));
}

//epicycloid

DCoordinate3 epicycloid::d0(GLdouble u)
{

    return DCoordinate3(r*(k+1)*cos(u) -r*cos((k+1)*u), r*(k+1)*sin(u) -r*sin((k+1)*u), 0 );
}

DCoordinate3 epicycloid::d1(GLdouble u)
{
    //GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(-r*(k+1)*sin(u)+r*(k+1)*sin((k+1)*u),r*(k+1)*cos(u)-r*(k+1)*cos((k+1)*u), 0);
}

DCoordinate3 epicycloid::d2(GLdouble u)
{
    //GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(-r*(k+1)*cos(u)+r*(k+1)*(k+1)*cos((k+1)*u), -r*(k+1)*sin(u)+r*(k+1)*(k+1)*sin((k+1)*u),0);
}

//hypocycloid

DCoordinate3 hypocycloid::d0(GLdouble u)
{

    return DCoordinate3(r*(k-1)*cos(u) +r*cos((k-1)*u), r*(k-1)*sin(u) -r*sin((k-1)*u), 0 );
}

DCoordinate3 hypocycloid::d1(GLdouble u)
{
    //GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(-r*(k-1)*sin(u)-r*(k-1)*sin((k-1)*u),r*(k-1)*cos(u)-r*(k-1)*cos((k-1)*u), 0);
}

DCoordinate3 hypocycloid::d2(GLdouble u)
{
    //GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(-r*(k-1)*cos(u)-r*(k-1)*(k-1)*cos((k-1)*u), r*(k-1)*sin(u)+r*(k-1)*(k-1)*sin((k-1)*u),0);
}

//viviani

DCoordinate3 viviani::d0(GLdouble u)
{

    return DCoordinate3(a*(1+cos(u)),a*sin(u),2*a*sin(u/2));
}

DCoordinate3 viviani::d1(GLdouble u)
{
    //GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(-a*sin(u),a*cos(u),a*cos(u/2));
}

DCoordinate3 viviani::d2(GLdouble u)
{
    //GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(-a*cos(u),-a*sin(u),-(1/2)*sin(u/2));
}

//torus

DCoordinate3 torus::d00(GLdouble u, GLdouble v)
{
    return DCoordinate3((R+r*sin(u))*cos(v),(R+r*sin(u))*sin(v),r*cos(u));
}

DCoordinate3 torus::d10(GLdouble u, GLdouble v)
{
    return DCoordinate3(r*cos(v)*cos(u), r*sin(v)*cos(u),-r*sin(u));
}

DCoordinate3 torus::d01(GLdouble u, GLdouble v)
{
    return DCoordinate3(-(R+r*sin(u))*sin(v),(R+r*sin(u))*cos(v),0);
}

//torus_circle

DCoordinate3 torus_circle::d00(GLdouble u, GLdouble v)
{
    return DCoordinate3((R+r*sin(u))*cos(v),(R+r*sin(u))*sin(v),r*cos(u));
}

DCoordinate3 torus_circle::d10(GLdouble u, GLdouble v)
{
    return DCoordinate3(r*cos(v)*cos(u), r*sin(v)*cos(u),-r*sin(u));
}

DCoordinate3 torus_circle::d01(GLdouble u, GLdouble v)
{
    return DCoordinate3(-(R+r*sin(u))*sin(v),(R+r*sin(u))*cos(v),0);
}

//alfred

DCoordinate3 alfred::d00(GLdouble u, GLdouble v)
{
    return DCoordinate3(3*cos(u)+1.0/2*(1+cos(2*u))*sin(v) - 1.0/2*sin(2*u)*sin(2*v)
                        ,3*sin(u)+1.0/2*sin(2*u)*sin(v)-1.0/2*(1-cos(2*u))*sin(2*v)
                        ,cos(u)*sin(2*v)+sin(u)*sin(v));
}

DCoordinate3 alfred::d10(GLdouble u, GLdouble v)
{
    return DCoordinate3(-sin(v)*sin(2*u)-sin(2*v)*cos(2*u)-3*sin(u),
                        -sin(2*v)*sin(2*u)+sin(v)*cos(2*u)+3*cos(u)
                        ,sin(v)*cos(u)-sin(2*v)*sin(u));
}

DCoordinate3 alfred::d01(GLdouble u, GLdouble v)
{
    return DCoordinate3((cos(2*u)+1)*cos(v)/2-sin(2*u)*cos(2*v),
                        sin(2*u)*cos(v)/2-(1-cos(2*u))*cos(2*v),
                        2*cos(u)*cos(2*v)+sin(u)*cos(v));
}

//helicoid

DCoordinate3 helicoid::d00(GLdouble u, GLdouble v)
{
    return DCoordinate3((2+u)*cos(v)
                        ,(2+u)*sin(v)
                        ,v);
}

DCoordinate3 helicoid::d10(GLdouble u, GLdouble v)
{
    return DCoordinate3(cos(v),
                        sin(v),
                        0);
}

DCoordinate3 helicoid::d01(GLdouble u, GLdouble v)
{
    return DCoordinate3(-(2+u)*sin(v),
                         (u+2)*cos(v),
                         1);
}

//hybrid

DCoordinate3 hybrid::d00(GLdouble u, GLdouble v)
{
    return DCoordinate3((1+cosh(u-3./2))*sin(v)
                        ,(1+cosh(u-3./2))*cos(v)
                        ,sinh(u-3./2) );
}

DCoordinate3 hybrid::d10(GLdouble u, GLdouble v)
{
    return DCoordinate3(sin(v)*sinh(u-3./2),
                        cos(v)*sinh(u-3./2),
                        cosh(u-3./2));
}

DCoordinate3 hybrid::d01(GLdouble u, GLdouble v)
{
    return DCoordinate3((cosh(u-3./2)+1)*cos(v),
                        -(cosh(u-3./2)+1)*sin(v),
                         0);
}
