#include <GL/glew.h>
#include "Core/BezierSurface3.h"
#include "Core/Matrices.h"
#include "Bezier/BicubicBezierPatches.h"

using namespace std;
using namespace cagd;


GLboolean BezierSurface3::InsertNewPatch(int x, int y, int pos)
{
    //GLuint l = _attribute_vector.size();
    cout <<"a";
    Attributes attr;
    cout <<"b";
    attr.patch = new BicubicBezierPatch();
    attr.patch->SetData(0, 0, -2.0, -2.0, 0.0);
    attr.patch->SetData(0, 1, -2.0, -1.0, 0.0);
    attr.patch->SetData(0, 2, -2.0, 1.0, 0.0);
    attr.patch->SetData(0, 3, -2.0, 2.0, 0.0);

    attr.patch->SetData(1, 0, -1.0, -2.0, 0.0);
    attr.patch->SetData(1, 1, -1.0, -1.0, 2.0);
    attr.patch->SetData(1, 2, -1.0, 1.0, 2.0);
    attr.patch->SetData(1, 3, -1.0, 2.0, 0.0);

    attr.patch->SetData(2, 0, 1.0, -2.0, 0.0);
    attr.patch->SetData(2, 1, 1.0, -1.0, 2.0);
    attr.patch->SetData(2, 2, 1.0, 1.0, 2.0);
    attr.patch->SetData(2, 3, 1.0, 2.0, 0.0);

    attr.patch->SetData(3, 0, 2.0, -2.0, 0.0);
    attr.patch->SetData(3, 1, 2.0, -1.0, 0.0);
    attr.patch->SetData(3, 2, 2.0, 1.0, 0.0);
    attr.patch->SetData(3, 3, 2.0, 2.0, 0.0);

    int len = _attribute_vector.size();
    DCoordinate3 main_pos (0.0, 0.0, 0.0);
    if( len >= 1)
    {
        DCoordinate3 move_with = DCoordinate3(x, y, 0);
        attr.patch->move(main_pos+move_with);

    }
    attr.mesh_after = new TriangulatedMesh3();
    attr.mesh_before = new TriangulatedMesh3();

    attr.mesh_before = attr.patch->GenerateImage(30,30,GL_STATIC_DRAW);

    if( attr.mesh_before )
        attr.mesh_before->UpdateVertexBufferObjects();

    //define an interpolation problem
    //1: creat a knot vector in u-direction
    RowMatrix<GLdouble> u_knot_vector(4);
    u_knot_vector(0) = 0.0;
    u_knot_vector(1) = 1.0 / 3.0;
    u_knot_vector(2) = 2.0 / 3.0;
    u_knot_vector(3) = 1.0;

    //2: create a knot vector in v-direction
    ColumnMatrix<GLdouble> v_knot_vector(4);
    v_knot_vector(0) = 0.0;
    v_knot_vector(1) = 1.0 / 3.0;
    v_knot_vector(2) = 2.0 / 3.0;
    v_knot_vector(3) = 1.0;

    //3: define a matrix of data points, eg. set them to the original control points
    Matrix<DCoordinate3> data_points_to_interpolate(4,4);
    for(GLuint row = 0; row < 4; ++row)
    {
        for(GLuint column = 0; column < 4; ++column)
        {
            attr.patch->GetData(row, column, data_points_to_interpolate(row,column));
        }
    }

    //4:solve the interpolation problem and generate the mesh of the interpolating patch

    if( attr.patch->UpdateDataForInterpolation(u_knot_vector,v_knot_vector, data_points_to_interpolate) )
    {
        attr.mesh_after = attr.patch->GenerateImage(30, 30, GL_STATIC_DRAW);
        if(attr.mesh_after)
            attr.mesh_after->UpdateVertexBufferObjects();
    }



    attr._neighbour.resize(8);
    for(GLuint i = 0; i < 8; ++i)
    {
        attr._neighbour[i] = 0;
    }
    //_attribute_vector.resize(9);
    _attribute_vector.push_back(attr);
    //_attribute_vector[len-1].id_patch = len-1;

    if( len > 1 )
    {
        addToNeighbours(len-1,_attribute_vector[len-1].patch, pos);
        joinNeighbours(len-1);
        join(0,_attribute_vector[len-1].patch, pos);
    }

    return GL_TRUE;

}

GLvoid BezierSurface3::Render() const
{
    for (GLuint i = 0; i < _attribute_vector.size(); ++i)
    {
        if(_attribute_vector[i].patch)
        {
            if(_attribute_vector[i].mesh_before)
            {
                MatFBRuby.Apply();
                _attribute_vector[i].mesh_before->Render();
            }

            if(_attribute_vector[i].mesh_after)
            {
                glEnable(GL_BLEND);
                glDepthMask(GL_FALSE);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE);
                    MatFBTurquoise.Apply();
                   _attribute_vector[i].mesh_after->Render();
                glDepthMask(GL_TRUE);
                glDisable(GL_BLEND);
            }
        }
    }
}

int BezierSurface3::getAttributeVectorLength()
{
    return _attribute_vector.size();
}

GLvoid BezierSurface3::movePatch(const DCoordinate3& rhs, int index)
{
    if ( _attribute_vector[index].patch )
        _attribute_vector[index].patch->move(rhs);

     _attribute_vector[index].mesh_before =  _attribute_vector[index].patch->GenerateImage(30,30,GL_STATIC_DRAW);

    if(  _attribute_vector[index].mesh_before )
         _attribute_vector[index].mesh_before->UpdateVertexBufferObjects();

    //define an interpolation problem
    //1: creat a knot vector in u-direction
    RowMatrix<GLdouble> u_knot_vector(4);
    u_knot_vector(0) = 0.0;
    u_knot_vector(1) = 1.0 / 3.0;
    u_knot_vector(2) = 2.0 / 3.0;
    u_knot_vector(3) = 1.0;

    //2: create a knot vector in v-direction
    ColumnMatrix<GLdouble> v_knot_vector(4);
    v_knot_vector(0) = 0.0;
    v_knot_vector(1) = 1.0 / 3.0;
    v_knot_vector(2) = 2.0 / 3.0;
    v_knot_vector(3) = 1.0;

    //3: define a matrix of data points, eg. set them to the original control points
    Matrix<DCoordinate3> data_points_to_interpolate(4,4);
    for(GLuint row = 0; row < 4; ++row)
    {
        for(GLuint column = 0; column < 4; ++column)
        {
            _attribute_vector[index].patch->GetData(row, column, data_points_to_interpolate(row,column));
        }
    }

    if( _attribute_vector[index].patch->UpdateDataForInterpolation(u_knot_vector,v_knot_vector, data_points_to_interpolate) )
    {
        _attribute_vector[index].mesh_after = _attribute_vector[index].patch->GenerateImage(30, 30, GL_STATIC_DRAW);
        if(_attribute_vector[index].mesh_after)
            _attribute_vector[index].mesh_after->UpdateVertexBufferObjects();
    }
}

GLboolean BezierSurface3::add_patch_simple(GLuint patch_array_index,GLuint to_patch, GLuint index, BicubicBezierPatch* patch)
{
    if (index < 8)
    {
//       if( _attribute_vector[to_patch+1].patch->getU_knot()  )
//           cout<<"letezik";
        if (_attribute_vector[patch_array_index].patch )
        {
            if ( _attribute_vector[patch_array_index]._neighbour[index])
            {
                delete _attribute_vector[patch_array_index]._neighbour[index];
                 _attribute_vector[patch_array_index]._neighbour[index] = 0;
            }
            //miert ad hibat???
           _attribute_vector[patch_array_index]._neighbour[index] = patch;
           return GL_TRUE;
        }
    }
    return GL_FALSE;
}

GLboolean BezierSurface3::add_to_neighbours(GLuint patch_index,
                                            GLuint neighbour_index,
                                            GLuint patch_direction,
                                            GLuint neighbour_direction,
                                            BicubicBezierPatch* patch,
                                            GLuint patch_array_index)
{
    if (patch_index < 8 && neighbour_index < 8)
    {
       if ( _attribute_vector[0]._neighbour[neighbour_index] != 0 && _attribute_vector[0]._neighbour[patch_index] != 0 )
       {
            add_patch_simple(patch_array_index, neighbour_index,patch_direction, patch);
            add_patch_simple(patch_array_index, patch_index,neighbour_direction, _attribute_vector[0]._neighbour[neighbour_index]);
       }
       return GL_TRUE;
    }
    return GL_FALSE;
}

GLvoid BezierSurface3::addToNeighbours(GLuint patch_array_index,BicubicBezierPatch* patch, int index)
{

    if( index < 8 )
    {
        //_attribute_vector[0]._neighbour[index] = bpp;

        switch(index)
        {
        case 0 :
            {

               add_to_neighbours(0, 7, 2, 6, patch, patch_array_index);
               add_to_neighbours(0, 1, 6, 2, patch, patch_array_index);
               add_to_neighbours(0, 6, 1, 5, patch, patch_array_index);
               add_to_neighbours(0, 2, 7, 3, patch, patch_array_index);
               add_patch_simple(patch_array_index,index, 4,  _attribute_vector[0].patch);
                break;
            }
        case 1: add_to_neighbours(1, 0, 2, 6, patch, patch_array_index);
                add_to_neighbours(1, 2, 0, 4, patch, patch_array_index);
                add_patch_simple(patch_array_index,index, 5,  _attribute_vector[0].patch);

                break;
        case 2: add_to_neighbours(2, 1, 4, 0, patch, patch_array_index);
                add_to_neighbours(2, 3, 0, 4, patch, patch_array_index);
                add_to_neighbours(2, 0, 3, 7, patch, patch_array_index);
                add_to_neighbours(2, 4, 1, 5, patch, patch_array_index);
                add_patch_simple(patch_array_index,index, 6,  _attribute_vector[0].patch);

                break;
        case 3: add_to_neighbours(3, 2, 4, 0, patch, patch_array_index);
                add_to_neighbours(3, 4, 2, 6, patch, patch_array_index);
                add_patch_simple(patch_array_index,index, 7,  _attribute_vector[0].patch);

                break;
        case 4: add_to_neighbours(4, 5, 2, 6, patch, patch_array_index);
                add_to_neighbours(4, 3, 6, 2, patch, patch_array_index);
                add_to_neighbours(4, 6, 3, 7, patch, patch_array_index);
                add_to_neighbours(4, 2, 5, 1, patch, patch_array_index);
                add_patch_simple(patch_array_index,index, 0,  _attribute_vector[0].patch);

                break;
        case 5: add_to_neighbours(5, 6, 4, 0, patch, patch_array_index);
                add_to_neighbours(5, 4, 6, 2, patch, patch_array_index);
                add_patch_simple(patch_array_index,index, 1,  _attribute_vector[0].patch);

                break;
        case 6: add_to_neighbours(6, 7, 4, 0, patch, patch_array_index);
                add_to_neighbours(6, 5, 0, 4, patch, patch_array_index);
                add_to_neighbours(6, 0, 5, 1, patch, patch_array_index);
                add_to_neighbours(6, 4, 7, 3, patch, patch_array_index);
                add_patch_simple(patch_array_index,index, 2,  _attribute_vector[0].patch);

                break;
        case 7: add_to_neighbours(7, 0, 6, 2, patch, patch_array_index);
                add_to_neighbours(7, 6, 0, 4, patch, patch_array_index);
                add_patch_simple(patch_array_index,index, 3,  _attribute_vector[0].patch);

                break;
        }

    }
}

GLvoid BezierSurface3::join(GLuint patch_array_index, BicubicBezierPatch* patch, GLuint pos)
{
    //patch_array_index beszurt patch helye es annak sorba a szomszedja

    if (_attribute_vector[patch_array_index]._neighbour[0])
    {
        GLuint row_count = _attribute_vector[patch_array_index]._neighbour[0]->_data.GetRowCount();
        GLuint col_count = _attribute_vector[patch_array_index]._neighbour[0]->_data.GetColumnCount();
        for (int i = 0; i < col_count; ++i)
        {
            (*_attribute_vector[patch_array_index]._neighbour[0]).SetData(i, 0, (*_attribute_vector[patch_array_index].patch)(i, row_count - 1));
            (*_attribute_vector[patch_array_index]._neighbour[0]).SetData(i, 1, 2 * (*_attribute_vector[patch_array_index].patch)(i, row_count - 1) - (*_attribute_vector[patch_array_index].patch)(i, row_count - 2));
        }
    }
    if (_attribute_vector[patch_array_index]._neighbour[1])
    {
        GLuint col_count = _attribute_vector[patch_array_index].patch->_data.GetColumnCount();
        GLuint row_count = (*_attribute_vector[patch_array_index]._neighbour[1])._data.GetRowCount();
        (*_attribute_vector[patch_array_index]._neighbour[1]).SetData(0, 0, (*_attribute_vector[patch_array_index].patch)(col_count - 1, row_count - 1));
        (*_attribute_vector[patch_array_index]._neighbour[1]).SetData(0, 1, 2 * (*_attribute_vector[patch_array_index].patch)(col_count - 1, row_count - 1) - (*_attribute_vector[patch_array_index].patch)(col_count - 1, row_count - 2));
        (*_attribute_vector[patch_array_index]._neighbour[1]).SetData(1, 0, 2 * (*_attribute_vector[patch_array_index].patch)(col_count - 1, row_count - 1) - (*_attribute_vector[patch_array_index].patch)(col_count - 2, row_count - 1));
    }
//    if (_neighbour_patches[2])
//    {
//        GLuint col_count = _data.GetColumnCount();
//        GLuint row_count =  _data.GetRowCount();
//        for (int i = 0; i < row_count; ++i)
//        {
//            (*_neighbour_patches[2]).SetData(0, i, (*this)(col_count - 1, i));
//            (*_neighbour_patches[2]).SetData(1, i, 2 * (*this)(col_count - 1, i) - (*this)(col_count - 2, i));
//        }
//    }
//    if (_neighbour_patches[3])
//    {
//        GLuint col_count = _data.GetColumnCount();
//        GLuint row_count = (*_neighbour_patches[3])._data.GetRowCount();
//        (*_neighbour_patches[3]).SetData(0, row_count - 1, (*this)(col_count - 1, 0));
//        (*_neighbour_patches[3]).SetData(0, row_count - 2, 2 * (*this)(col_count - 1, 0) - (*this)(col_count - 1, 1));
//        (*_neighbour_patches[3]).SetData(1, row_count - 1, 2 * (*this)(col_count - 1, 0) - (*this)(col_count - 2, 0));
//    }
//    if (_neighbour_patches[4])
//    {
//        GLuint row_count = (*_neighbour_patches[4])._data.GetRowCount();
//        GLuint col_count = _data.GetColumnCount();
//        for (int i = 0; i < col_count; ++i)
//        {
//            (*_neighbour_patches[4]).SetData(i, row_count - 1, (*this)(i, 0));
//            (*_neighbour_patches[4]).SetData(i, row_count - 2, 2 * (*this)(i, 0) - (*this)(i, 1));
//        }
//    }
//    if (_neighbour_patches[5])
//    {
//        GLuint col_count = (*_neighbour_patches[5])._data.GetColumnCount();
//        GLuint row_count = _data.GetRowCount();
//        (*_neighbour_patches[5]).SetData(col_count - 1, row_count - 1, (*this)(0, 0));
//        (*_neighbour_patches[5]).SetData(col_count - 1, row_count - 2, 2 * (*this)(0, 0) - (*this)(0, 1));
//        (*_neighbour_patches[5]).SetData(col_count - 2, row_count - 1, 2 * (*this)(0, 0) - (*this)(1, 0));
//    }
//    if (_neighbour_patches[6])
//    {
//        GLuint col_count = _data.GetColumnCount();
//        GLuint row_count =  _data.GetRowCount();
//        for (int i = 0; i < row_count; ++i)
//        {
//            (*_neighbour_patches[6]).SetData(col_count - 1, i, (*this)(0, i));
//            (*_neighbour_patches[6]).SetData(col_count - 2, i, 2 * (*this)(0, i) - (*this)(1, i));
//        }
//    }
//    if (_neighbour_patches[7])
//    {
//        GLuint col_count = _data.GetColumnCount();
//        GLuint row_count = (*_neighbour_patches[7])._data.GetRowCount();
//        (*_neighbour_patches[7]).SetData(col_count - 1, 0, (*this)(0, row_count - 1));
//        (*_neighbour_patches[7]).SetData(col_count - 1, 1, 2 * (*this)(0, row_count - 1) - (*this)(0, row_count - 2));
//        (*_neighbour_patches[7]).SetData(row_count - 2, 0, 2 * (*this)(0, row_count - 1) - (*this)(1, row_count - 1));
//    }


}

GLvoid BezierSurface3::joinNeighbours(GLuint patch_array_index)
{
    for (int i = 0; i < 8; ++i)
    {
        if ( _attribute_vector[patch_array_index]._neighbour[i] )
        {
            //_neighbour_patches[i]->join(_attribute_vector[patch_array_index]._neighbour[i]);
            //patch_array_index beszurt patch helye es annak sorba a szomszedja
            join(patch_array_index, _attribute_vector[patch_array_index].patch, i);
            _attribute_vector[patch_array_index]._neighbour[i]->GenerateImage(30,30,GL_STATIC_DRAW);
            // attr.mesh_before = attr.patch->GenerateImage(30,30,GL_STATIC_DRAW);
            _attribute_vector[patch_array_index]._neighbour[i]->UpdateVertexBufferObjectsOfData();

//            _attribute_vector[patch_array_index].mesh_before = _attribute_vector[patch_array_index]._neighbour[i]->GenerateImage(30,30,GL_STATIC_DRAW);
//            if ( _attribute_vector[patch_array_index].mesh_before )
//                _attribute_vector[patch_array_index].mesh_before->UpdateVertexBufferObjects();

        }
    }
}

BicubicBezierPatch* BezierSurface3::getPatch(int index)
{
    return _attribute_vector[index].patch;
}

