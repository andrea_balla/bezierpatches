#pragma once

#include <GL/glew.h>
#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <Bezier/BicubicBezierPatches.h>
#include <Core/TriangulatedMeshes3.h>
#include <Core/Materials.h>
#include <Core/ShaderPrograms.h>
#include <Core/Matrices.h>

using namespace std;
namespace cagd
{
    class BezierSurface3
    {
        public:
        class Attributes
            {
                public:
                    BicubicBezierPatch  *patch;
                    GLuint id_patch;
                    TriangulatedMesh3   *mesh_before, *mesh_after;
                    Material            *material;
                    ShaderProgram       *shader;
                    vector<BicubicBezierPatch*> _neighbour;

                    //GLboolean add_patch_simple(GLuint to_patch, GLuint index, BicubicBezierPatch* patch);
            };

        protected:
            vector<Attributes> _attribute_vector;
        public:
        GLboolean InsertNewPatch(int x = 0, int y = 0, int pos = 0);
        GLvoid Render() const;
        int getAttributeVectorLength();
        GLvoid movePatch(const DCoordinate3& rhs, int index);
        GLvoid addToNeighbours(GLuint patch_array_index, BicubicBezierPatch* bpp, int index);
        GLvoid joinNeighbours(GLuint patch_array_index);
        GLvoid join(GLuint patch_array_index, BicubicBezierPatch* patch, GLuint pos);
        BicubicBezierPatch* getPatch(int index);
        GLboolean add_to_neighbours(GLuint patch_index,
                                    GLuint neighbour_index,
                                    GLuint patch_direction,
                                    GLuint neighbour_direction,
                                    BicubicBezierPatch* patch,
                                    GLuint patch_array_index);

        GLboolean add_patch_simple(GLuint patch_array_index,GLuint to_patch, GLuint index, BicubicBezierPatch* patch);
    };


}
