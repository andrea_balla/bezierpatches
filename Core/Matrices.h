#pragma once

#include <iostream>
#include <vector>
#include <GL/glew.h>

namespace cagd
{
    // forward declaration of template class Matrix
    template <typename T>
    class Matrix;

    // forward declaration of template class RowMatrix
    template <typename T>
    class RowMatrix;

    // forward declaration of template class ColumnMatrix
    template <typename T>
    class ColumnMatrix;

    // forward declarations of overloaded and templated input/output from/to stream operators
    template <typename T>
    std::ostream& operator << (std::ostream& lhs, const Matrix<T>& rhs);

    template <typename T>
    std::istream& operator >>(std::istream& lhs, Matrix<T>& rhs);

    //----------------------
    // template class Matrix
    //----------------------
    template <typename T>
    class Matrix
    {
        friend std::ostream& cagd::operator << <T>(std::ostream&, const Matrix<T>& rhs);
        friend std::istream& cagd::operator >> <T>(std::istream&, Matrix<T>& rhs);

    protected:
        unsigned int                    _row_count;
        unsigned int                    _column_count;
        std::vector< std::vector<T> >   _data;
    public:
        // special constructor (can also be used as a default constructor)
        Matrix(unsigned int row_count = 1,
               unsigned int column_count = 1);

        // copy constructor
        Matrix(const Matrix& m);

        // assignment operator
        Matrix& operator =(const Matrix& m);

        // get element by reference
        T& operator ()(unsigned int row, unsigned int column);

        // get copy of an element
        T operator ()(unsigned int row, unsigned int column) const;

        // get dimensions
        unsigned int GetRowCount() const;
        unsigned int GetColumnCount() const;

        // set dimensions
        virtual GLboolean ResizeRows(unsigned int row_count);
        virtual GLboolean ResizeColumns(unsigned int column_count);

        // updates
        GLboolean SetRow(unsigned int index, const RowMatrix<T>& row);
        GLboolean SetColumn(unsigned int index, const ColumnMatrix<T>& column);

        // destructor
        virtual ~Matrix();
    };

    //-------------------------
    // template class RowMatrix
    //-------------------------
    template <typename T>
    class RowMatrix: public Matrix<T>
    {
    public:
        // special constructor (can also be used as a default constructor)
        RowMatrix(unsigned int column_count = 1);

        // get element by reference
        T& operator ()(unsigned int column);
        T& operator [](unsigned int column);

        // get copy of an element
        T operator ()(unsigned int column) const;
        T operator [](unsigned int column) const;

        // a row matrix consists of a single row
        GLboolean ResizeRows(unsigned int row_count);
    };

    //----------------------------
    // template class ColumnMatrix
    //----------------------------
    template <typename T>
    class ColumnMatrix: public Matrix<T>
    {
    public:
        // special constructor (can also be used as a default constructor)
        ColumnMatrix(unsigned int row_count = 1);

        // get element by reference
        T& operator ()(unsigned int row);
        T& operator [](unsigned int row);

        // get copy of an element
        T operator ()(unsigned int row) const;
        T operator [](unsigned int row) const;

        // a column matrix consists of a single column
        GLboolean ResizeColumns(unsigned int column_count);
    };

    //--------------------------------------------------
    // homework: implementation of template class Matrix
    //--------------------------------------------------


    template <typename T>
    Matrix<T>& Matrix<T>::operator =(const Matrix<T>& m)
    {
        if (this != &m)
        {
            _row_count = m._row_count;
            _column_count = m._column_count;
            _data = m._data;
        }
        return *this;
    }

    // get element by reference
    template <typename T>
    T& Matrix<T>::operator ()(unsigned int row, unsigned int column)
    {
        return _data[row][column];
    }

    // get copy of an element
    template <typename T>
    T Matrix<T>::operator ()(unsigned int row, unsigned int column) const
    {
        return _data[row][column];
    }

    // get dimensions
    template <typename T>
    unsigned int Matrix<T>::GetRowCount() const
    {
        return _row_count;
    }

    template <typename T>
    unsigned int Matrix<T>::GetColumnCount() const
    {
        return _column_count;
    }

    // set dimensions
    template <typename T>
    GLboolean Matrix<T>::ResizeRows(unsigned int row_count)
    {
        _data.resize(row_count);

        for (GLuint r = _row_count; r < row_count; r++)
            _data[r].resize(_column_count);
        _row_count = row_count;

        return GL_TRUE;
    }

    template <typename T>
    GLboolean Matrix<T>::ResizeColumns(unsigned int column_count)
    {
        _column_count = column_count;

        for (GLuint r = 0; r < _row_count; r++)
            _data[r].resize(_column_count);

        return GL_TRUE;
    }

    // update
    template <typename T>
    GLboolean Matrix<T>::SetRow(unsigned int index, const RowMatrix<T>& row)
    {
        if (_column_count != row._column_count || index >= _row_count)
            return GL_FALSE;

        _data[index] = row._data[0];

        return GL_TRUE;
    }

    template <typename T>
    GLboolean Matrix<T>::SetColumn(unsigned int index, const ColumnMatrix<T>& column)
    {
        if (_row_count != column._row_count || index >= _column_count)
            return GL_FALSE;

        for (GLuint r = 0; r < _row_count; r++)
            _data[r][index] = column._data[r][0];

        return GL_TRUE;
    }

    // destructor
    template <typename T>
    Matrix<T>::~Matrix()
    {
        _data.clear();
    }

    //special constructor
    template <typename T >
    inline Matrix<T>::Matrix(unsigned int row_count, unsigned int column_count)
    {
       _data.resize(row_count);
       _row_count = row_count;
       _column_count = column_count;

        for (GLuint i = 0; i < _data.size(); i++) {
            _data[i].resize(column_count);
        }

    }

    // copy constructor
    template <typename T >
    inline Matrix<T>::Matrix(const Matrix<T>& m)
    {
        _data = m._data;
        _row_count = m.GetRowCount();
        _column_count = m.GetColumnCount();
    }


//    // destructor

//    template<typename T>
//    inline Matrix<T>::~Matrix() {}


//    // assignment operator
//    template <typename T >
//    Matrix<T>& Matrix<T>::operator =(const Matrix<T>& m)
//    {
//        if (&m == this)
//           return *this;

//         unsigned new_rows = m.GetRowCount();
//         unsigned new_cols = m.GetColumnCount();

//         _data.resize(new_rows);
//         for (GLuint i=0; i<_data.size(); i++) {
//           _data[i].resize(new_cols);
//         }

//         for (GLuint i = 0; i  < new_rows; i++) {
//           for (GLuint j = 0; j < new_cols; j++) {
//             _data[i][j] = m(i, j);
//           }
//         }
//         _row_count = new_rows;
//         _column_count = new_cols;

//         return *this;
//    }

//    // get element by reference
//    template < typename T >
//    inline T& Matrix<T>::operator ()(unsigned int row, unsigned int column)
//    {
//        return  *this->_data[row][column];
//    }

//    // get copy of an element
//    template < typename T >
//    inline T Matrix<T>::operator () (GLuint row , GLuint column ) const
//    {
//        return this->_data[row][column];
//    }

//    // get dimensions
//    template < typename T >
//    unsigned int Matrix<T>::GetRowCount() const
//    {
//        return this->_row_count;
//    }

//    template < typename T >
//    unsigned int Matrix<T>::GetColumnCount() const
//    {
//        return this->_column_count;
//    }

//    // set dimensions
//    template < typename T >
//    GLboolean Matrix<T>::ResizeRows(unsigned int row_count)
//    {
//        _row_count = row_count;
//        for (GLuint i = 0; i < _row_count; i++) {
//            _data[i].resize(_row_count);
//        }
//       return GL_TRUE;
//    }

//    template < typename T >
//    GLboolean Matrix<T>::ResizeColumns(unsigned int column_count)
//    {
//        _column_count = column_count;
//        for (GLuint i = 0; i <_column_count; i++) {
//            _data[i].resize(_column_count);
//        }
//        return GL_TRUE;
//    }

//    // updates
//    template < typename T >
//    GLboolean Matrix<T>::SetRow(unsigned int index, const RowMatrix<T>& row)
//    {
//        return GL_TRUE;
//    }
//    template < typename T >
//    GLboolean Matrix<T>::SetColumn(unsigned int index, const ColumnMatrix<T>& column)
//    {
//        return GL_TRUE;
//    }

    //-----------------------------------------------------
    // homework: implementation of template class RowMatrix
    //-----------------------------------------------------



    // special constructor (can also be used as a default constructor)
        template <typename T>
        RowMatrix<T>::RowMatrix(unsigned int column_count): Matrix<T>(1, column_count)
        {}

        // get element by reference
        template <typename T>
        T& RowMatrix<T>::operator ()(unsigned int column)
        {
            return this->_data[0][column];
        }

        template <typename T>
        T& RowMatrix<T>::operator [](unsigned int column)
        {
            return this->_data[0][column];
        }

        // get copy of an element
        template <typename T>
        T RowMatrix<T>::operator ()(unsigned int column) const
        {
            return this->_data[0][column];
        }

        template <typename T>
        T RowMatrix<T>::operator [](unsigned int column) const
        {
            return this->_data[0][column];
        }

        // a row matrix consists of a single row
        template <typename T>
        GLboolean RowMatrix<T>::ResizeRows(unsigned int row_count)
        {
            return (row_count == 1 ? GL_TRUE:GL_FALSE);
        }


    // special constructor (can also be used as a default constructor)
//    template < typename T >
//    inline RowMatrix<T>::RowMatrix(unsigned int column_count)
//    {
//        this->_column_count = column_count;
//        for (GLuint i = 0; i < this->_data.size(); i++) {
//           this->_data[i].resize(column_count);
//        }

//    }

//    // get element by reference
//    template < typename T >
//    inline T& RowMatrix<T>::operator ()(unsigned int column)
//    {
//        return *this->Matrix<T>::_data[0][column];
//    }
//    template < typename T >
//    inline T& RowMatrix<T>::operator [](unsigned int column)
//    {
//        return *this->Matrix<T>::_data[0][column];
//    }

//    // get copy of an element
//    template < typename T >
//    inline T RowMatrix<T>::operator ()(unsigned int column) const
//    {
//        return this-> Matrix<T>::_data[0][column];
//    }
//    template < typename T >
//    inline T  RowMatrix<T>::operator [](unsigned int column) const
//    {
//        return this-> Matrix<T>::_data[0][column];
//    }

//    // a row matrix consists of a single row
//    template < typename T >
//    GLboolean RowMatrix<T>::ResizeRows(unsigned int row_count)
//    {
//        Matrix<T>::_row_count = row_count;
//        for (GLuint i = 0; i < Matrix<T>::_row_count; i++) {
//            Matrix<T>::_data[i].resize(row_count);
//        }
//       return GL_TRUE;

//    }

    //--------------------------------------------------------
    // homework: implementation of template class ColumnMatrix
    //--------------------------------------------------------


        // special constructor (can also be used as a default constructor)
        template <typename T>
        ColumnMatrix<T>::ColumnMatrix(unsigned int row_count):Matrix<T>(row_count, 1)
        {
        }

        // get element by reference
        template <typename T>
        T& ColumnMatrix<T>::operator ()(unsigned int row)
        {
            return this->_data[row][0];
        }

        template <typename T>
        T& ColumnMatrix<T>::operator [](unsigned int row)
        {
            return this->_data[row][0];
        }

        // get copy of an element
        template <typename T>
        T ColumnMatrix<T>::operator ()(unsigned int row) const
        {
            return this->_data[row][0];
        }

        template <typename T>
        T ColumnMatrix<T>::operator [](unsigned int row) const
        {
            return this->_data[row][0];
        }

        // a column matrix consists of a single column
        template <typename T>
        GLboolean ColumnMatrix<T>::ResizeColumns(unsigned int column_count)
        {
            return (column_count == 1 ? GL_TRUE:GL_FALSE);
        }

//    template < typename T >
//    inline ColumnMatrix<T>::ColumnMatrix(unsigned int row_count)
//    {
//        Matrix<T>::_row_count = row_count;
//        Matrix<T>::_data.resize(row_count);
//    }

//    // get element by reference
//    template < typename T >
//    inline T& ColumnMatrix<T>::operator ()(unsigned int row)
//    {
//        return *this->Matrix<T>::_data[row][0];
//    }
//    template < typename T >
//    inline T& ColumnMatrix<T>::operator [](unsigned int row)
//    {
//        return *this->Matrix<T>::_data[row][0];
//    }

//    // get copy of an element
//    template < typename T >
//    inline T ColumnMatrix<T>::operator ()(unsigned int row) const
//    {
//        return this->Matrix<T>::_data[row][0];
//    }
//    template < typename T >
//    inline T ColumnMatrix<T>::operator [](unsigned int row) const
//    {
//        return this->Matrix<T>::_data[row][0];
//    }

//    // a column matrix consists of a single column
//    template < typename T >
//    GLboolean ColumnMatrix<T>::ResizeColumns(unsigned int column_count)
//    {
//       Matrix<T>::_column_count = column_count;
//        for (GLuint i = 0; i <Matrix<T>::_column_count; i++) {
//            Matrix<T>::_data[i].resize(column_count);
//        }
//        return GL_TRUE;
//    }

    //------------------------------------------------------------------------------
    // definitions of overloaded and templated input/output from/to stream operators
    //------------------------------------------------------------------------------

    // output to stream
    template <typename T>
    std::ostream& operator <<(std::ostream& lhs, const Matrix<T>& rhs)
    {
        lhs << rhs._row_count << " " << rhs._column_count << std::endl;
        for (typename std::vector< std::vector<T> >::const_iterator row = rhs._data.begin();
             row != rhs._data.end(); ++row)
        {
            for (typename std::vector<T>::const_iterator column = row->begin();
                 column != row->end(); ++column)
                    lhs << *column << " ";
            lhs << std::endl;
        }
        return lhs;
    }

    // input from stream
    //template <typename T>
    //std::istream& operator >>(std::istream& lhs, Matrix<T>& rhs)
    //{
        // homework
//        lhs >> rhs._row_count >> rhs._column_count;
//        rhs._data.resize(rhs._row_count);
//        for (GLuint r=0;  r< rhs._row_count; r++)
//        {
//            rhs._data[r].resize(rhs._column_count);
//            for(GLuint c = 0; c < rhs._column_count; c++)
//            {
//                lhs >> rhs._data[r][c];
//            }
//        }

//        for(std::vector< std::vector<T> >::iterator r = rhs._data.begin(); r != rhs._data.end(); r++)
//        {
//            r->resize(rhs._column_count);
//            for (std::vector<T>::iterator c =r->begin(); c!= r->end(); c++)
//            {
//                lhs >> *c;
//            }
//        }

//       return lhs;
    //}


    // input from stream
    template <typename T>
    std::istream& operator >>(std::istream& lhs, Matrix<T>& rhs)
    {
        lhs >> rhs._row_count >> rhs._column_count;
        rhs._data.resize(rhs._row_count);

        for (typename std::vector< std::vector<T> >::iterator row = rhs._data.begin();
             row != rhs._data.end(); ++row)
        {
            row->resize(rhs._column_count);
            for (typename std::vector<T>::iterator column = row->begin();
                 column != row->end(); ++column)
                    lhs >> *column;
        }
        return lhs;
    }

    // forward declaration of template class TriangularMatrix
       template <typename T>
       class TriangularMatrix;

       // forward declarations of overloaded and templated input/output from/to stream operators
      // ...

       template <typename T>
       std::istream& operator >>(std::istream& lhs, TriangularMatrix<T>& rhs);

       template <typename T>
       std::ostream& operator << (std::ostream& lhs, const TriangularMatrix<T>& rhs);

       //...

       //--------------------------------
       // template class TriangularMatrix
       //--------------------------------
       template <typename T>
       class TriangularMatrix
       {
           friend std::istream& cagd::operator >> <T>(std::istream&, TriangularMatrix<T>& rhs);
           friend std::ostream& cagd::operator << <T>(std::ostream&, const TriangularMatrix<T>& rhs);

       protected:
           GLuint                        _row_count;
           std::vector< std::vector<T> > _data;

       public:
           // special constructor (can also be used as a default constructor)
           TriangularMatrix(GLuint row_count = 1);

           // get element by reference
           T& operator ()(GLuint row, GLuint column);

           // get copy of an element
           T operator ()(GLuint row, GLuint column) const;

           // get dimension
           GLuint GetRowCount() const;

           // set dimension
           GLboolean ResizeRows(GLuint row_count);
       };

       template <typename T>
       TriangularMatrix<T>::TriangularMatrix(GLuint row_count): _row_count(row_count)
       {
            _data.resize(_row_count);

            for (GLuint r = 0; r < _row_count; ++r)
            {
                _data[r].resize(r + 1);
            }
       }

       template <typename T>
       T& TriangularMatrix<T>::operator ()(GLuint row, GLuint column)
       {
           return this->_data[row][column];
       }


       template <typename T>
       T TriangularMatrix<T>::operator ()(GLuint row, GLuint column) const
       {
           return this->_data[row][column];
       }


       template <typename T>
       GLuint TriangularMatrix<T>::GetRowCount() const
       {
           return this->_data.size();
       }

       template <typename T>
       GLboolean TriangularMatrix<T>::ResizeRows(GLuint row_count)
       {
           _data.resize(row_count);

           for (GLuint r = _row_count; r < row_count; ++r)
           {
               _data[r].resize(r + 1);
           }

           return GL_TRUE;
       }
}
